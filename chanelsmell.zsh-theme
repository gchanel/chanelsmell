# Color shortcuts
R=$fg[red]
G=$fg[green]
M=$fg[magenta]
RB=$fg_bold[red]
YB=$fg_bold[yellow]
BB=$fg_bold[blue]

if [ "$(whoami)" = "root" ]; then
    PROMPTCOLOR="%{$RB%}" PREFIX="-!-";
else
    PROMPTCOLOR="" PREFIX="---";
fi

local return_code="%(?..%{$RB%}%? ↵%{$reset_color%})"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$bg[magenta]%}%{$fg_bold[yellow]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$bg[magenta]%}%{$fg_bold[yellow]%}»%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_bold[red]%}!%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_AHEAD="%{$fg_bold[yellow]%}+%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[green]%}=%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}+"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[blue]%}m"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}x"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[magenta]%}r"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[yellow]%}="
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[cyan]%}*"

#HASH_COLOR_CODE=$((0x$(print "$(whoami)$(hostname)" | md5sum | cut -c1,2)))
TXT_COLOR="%{$fg[white]%}"
NAME_COLOR="%{$bg[black]%}"
#NAME_COLOR="%{$BG[$HASHED_COLOR_CODE]%}"
PWD_COLOR="%{$bg[blue]%}"

PROMPT='$NAME_COLOR$TXT_COLOR%n@%m»$PWD_COLOR%3~»$(git_prompt_info)%{$reset_color%} '
RPROMPT='${return_code}$(git_prompt_status)%{$reset_color%}[%{$fg_no_bold[yellow]%}%h%{$reset_color%}][%*]'
